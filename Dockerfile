FROM ubuntu:18.04

MAINTAINER Hana, <1586606701@qq.com>

ENV DEBIAN_FRONTEND=noninteractive


# 安装依赖和代码
RUN apt update && \
    apt upgrade -y && \
    apt install -y liblttng-ust0 libcurl4 libssl1.0.0 libkrb5-3 zlib1g libicu60 libunwind8 wget && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists && \
    useradd -d /home/container -m container && \
    wget https://dot.net/v1/dotnet-install.sh && \
    chmod 775 dotnet-install.sh && \
    ./dotnet-install.sh -c Current -Runtime dotnet -InstallDir /etc/dotnet && \
    ln -s /etc/dotnet/dotnet /usr/local/bin
    
    
USER container
WORKDIR /home/container

ENV  USER container
ENV  HOME /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]